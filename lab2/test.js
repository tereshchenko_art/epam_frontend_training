/**
 * Created by Администратор on 21.01.2016.
 */
describe("Testing our parseString() function", function(){
    it("test the whole function",function(){
       expect(parseString("wenodegotapplethispieclue","node apple pie")).toEqual("we got this clue");
    });
});
describe("Testing our parseString() function", function(){
    it("test the whole function",function(){
        expect(parseString("iphplikec++js","php c++")).toEqual("i like js");
    });
});
describe("Testing the part of",function(){
    it("numbers of spaces in second argument have to be  to numbers of spaces in output-1", function () {
    var input = "magiccanisofwhatgoiasreallywedo";
        var dividers = "can of go as we"
        var result = parseString(input,dividers);
        function countSpaces(string){
            var counter = 0;
            for (var i = 0; i<string.length; i++){
                if(string[i] == " "){
                    counter++
                }
            }
            return counter;
        }
        expect(countSpaces(dividers)).toEqual(countSpaces(result)-1);
 });
});
