function Point(x,y){
    this.x = x;
    this.y = y;
}
function Triangle(A,B,C){
    this.A = A;
    this.B = B;
    this.C = C;
}
function getTriangleProperties(triangle){
    var x1,x2,x3,y1,y2,y3;
    var Apoint, Bpoint, Cpoint;

    Apoint  = triangle.A;
    Bpoint  = triangle.B;
    Cpoint  = triangle.C;

    x1 = +Apoint.x;
    x2 = +Bpoint.x;
    x3 = +Cpoint.x;

    y1 = +Apoint.y;
    y2 = +Bpoint.y;
    y3 = +Cpoint.y;

    var AB  = +Math.sqrt(Math.pow((x2 - x1),2) + Math.pow((y2 - y1),2));
    var AC = +Math.sqrt(Math.pow((x3 - x1),2) + Math.pow((y3 - y1),2));
    var BC = +Math.sqrt(Math.pow((x3 - x2),2) + Math.pow((y3 - y2),2));
    var perimeter = AB + BC + AC;

    var p =+perimeter/2;
    var S = Math.sqrt(p*((p-AB)+(p-AC)+(p-BC)));
    return {Perimeter: perimeter, Square : S};
}


function init() {
    var x1, x2, x3, y1, y2, y3;
    x1 = +document.getElementById("x1").value;
    x2 = +document.getElementById("x2").value;
    x3 = +document.getElementById("x3").value;
    y1 = +document.getElementById("y1").value;
    y2 = +document.getElementById("y2").value;
    y3 = +document.getElementById("y3").value;
    var A = new Point(x1, y1);
    var B = new Point(x2, y2);
    var C = new Point(x3, y3);
    var ABC = new Triangle(A, B, C);
    var triangleProperties = getTriangleProperties(ABC);

    var Square = triangleProperties.Square;
    var Perimeter = triangleProperties.Perimeter;
    document.getElementById("perimeter").innerHTML = Perimeter;
    document.getElementById("square").innerHTML = Square;


}