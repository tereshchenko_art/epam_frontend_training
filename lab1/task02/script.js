function isDigitsNotEquals(digit){
    //we could parse this digit using % and / operations but it will be easier to do it with string
    var input = digit;
    var array = [];
    for (var i = 0; i<input.length; i++){
        array.push(input.charAt(i));
    }
    for (var i = 0; i<array.length-1; i++){
        if (array[i]!=array[i+1]){
            return false;
        }
    }
    return true;
}
function count(){
    var output = document.getElementById("output");
    var input = document.getElementById("input").value;
    output.innerHTML = isDigitsNotEquals(input);
}