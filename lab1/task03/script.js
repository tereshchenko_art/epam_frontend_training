function addPoint(){
    var point = document.createElement("div");
    var span_x = document.createElement("span");
    span_x.innerHTML = "x";
    var span_y = document.createElement("span");
    span_y.innerHTML = "y";
    var input_x = document.createElement("input");
    input_x.type = "number";
    input_x.className = "x";
    var input_y = document.createElement("input");
    input_x.type = "number";
    input_y.className = "y"
    point.appendChild(span_x);
    point.appendChild(input_x);
    point.appendChild(span_y);
    point.appendChild(input_y);
    var body = document.getElementById("body");
    body.appendChild(point);
}
function Point(x,y){
    this.x = x;
    this.y = y;
}
function init(){
    var points = [];
    var x,y = +0;
    var inputs = document.getElementsByTagName("input");
    var output = document.getElementById("output");
    for (var i = 0; i<inputs.length; i++){
        if(inputs[i].className == "x"){
            x = +inputs[i].value;
        }
        else{
            y = +inputs[i].value;
        }
        points.push(new Point(x,y));
    }
    for(var j = 0; j < points.length; j++){
        if(!checkSimmetry(points[j])){
            output.innerHTML = "points are not symmetric";
        }
        else{
            output.innerHTML = "points are symmetric";
        }

    }
 }
function checkSimmetry(point){
    var x,y;
    if (point.x < 0){
        x = -point.x;
    }
    else{
        x = point.x;
    }
    if (point.y < 0){
        y = -point.y;
    }
    else{
        y = point.y;
    }

    return  (x == y);

}